<h3><?= $title ?></h3>
<form action="/login/registration">
    <div class="col-12">
        <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label>Username</label>
        <input 
        class="form-control"
        name="user"
        placeholder="Enter username"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>First Name</label>
        <input
        class="form-control"
        name="firstname"
        placeholder="Enter first name"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Last Name</label>
        <input
        class="form-control"
        name="lastname"
        placeholder="Enter last name"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Password</label>
        <input
        class="form-control"
        name="password"
        type="password"
        placeholder="Enter password"
        maxlength="30">
    </div>
    <div class="form-group">
        <label>Repeat password</label>
        <input
        class="form-control"
        name="repeatpassword"
        type="password"
        placeholder="Repeat password"
        maxlength="30">
    </div>
    <button class="btn btn-primary">Register</button>
    <?= anchor('login/login','Login') ?>
</form>